DROP SCHEMA PUBLIC CASCADE;

CREATE SEQUENCE student_sequence AS INTEGER START WITH 1;

CREATE TABLE students (
  id BIGINT NOT NULL PRIMARY KEY,
  student_name VARCHAR(255),
  student_time VARCHAR(255),
  test_date VARCHAR(255),
  apache VARCHAR(255),
  mariaDB VARCHAR(255),
  php VARCHAR(255),
  web_manager VARCHAR(255)
);