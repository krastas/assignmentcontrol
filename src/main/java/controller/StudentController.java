package controller;

import dao.StudentDao;
import model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentDao dao;

    @PostMapping("students")
    public void insertStudent () {
        dao.insertStudent();
    }

    @GetMapping("students")
    public List<Student> getStudents() {
        return dao.getStudentList();
    }

}
