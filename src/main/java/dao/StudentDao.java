package dao;

import model.Student;

import java.util.List;

public interface StudentDao {

    void insertStudent();

    List<Student> getStudentList();

}
