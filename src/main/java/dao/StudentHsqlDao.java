package dao;

import data.GetStudentData;
import model.Student;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Repository
public class StudentHsqlDao implements StudentDao {
    GetStudentData getStudentData = new GetStudentData();
    ArrayList<Student> studentsList = getStudentData.studentData();
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void insertStudent() {
        Student newStudent;
        for (int i = 0; i < studentsList.size(); i++) {
            newStudent = studentsList.get(i);
            if (newStudent.getId() == null) {
                em.persist(newStudent);
            } else {
                em.merge(newStudent);
            }
        }
    }

    @Override
    public List<Student> getStudentList() {

        return em.createQuery("select s from Student s",
                Student.class).getResultList();
    }
}