package data;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DateTimeInfo {

    public static Long calculateTimeDiff(String startTime, String endTime) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date startingTime = format.parse(startTime);
        Date endingTime = format.parse(endTime);
        Long timeDiff = endingTime.getTime() - startingTime.getTime();
        Long diffMinutes = (timeDiff/60000);
        return diffMinutes;
    }

    public static String formatStartDate(String startDate) throws IOException {
        String dateMonth = startDate.substring(0, 3); //take out the month part
        DateTimeFormatter parser = DateTimeFormatter.ofPattern("MMM")
                .withLocale(Locale.ENGLISH);
        TemporalAccessor accessor = parser.parse(dateMonth);
        Integer nr = accessor.get(ChronoField.MONTH_OF_YEAR); //parse string month to number
        String date = nr.toString() + "/" + startDate.substring(4, 6); //put together numeric date
        return date;
    }

    public static Long CalculateDateDiff(String startDate, String endDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd", Locale.ENGLISH); //mark the date format
        Date firstDate = null;
        Date secondDate = null;
        try {
            firstDate = sdf.parse(startDate);
            secondDate = sdf.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime()); //time difference in milliseconds
        Long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS); //milliseconds to minutes
        return diff;
    }
}
