package data;

import config.DbConfig;
import dao.StudentDao;
import model.Student;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*idea from here: https://stackoverflow.com/questions/326390/how-do-i-create-a-java-string-from-the-contents-of-a-file
and here : https://stackoverflow.com/questions/8693417/read-multiple-data-in-multiple-files */

public class GetStudentData {

    public static ArrayList<Student> studentData() {
        Student student;
        ArrayList<Student> studentsList = new ArrayList<>();
        Pattern dateTimePattern = Pattern.compile("[A-Za-z]{3}[ ][0-9]{2}[ ][0-9]{2}[:][0-9]{2}");
        Pattern endTimePattern = Pattern.compile("[0-9]{2}\\/[/0-9]{2}#[0-9]{2}[:][0-9]{2}");
        Pattern apachePattern = Pattern.compile("Apache/[0-9]{1,2}\\.[0-9]{1}\\.[0-9]{1,2}");
        Pattern databasePattern = Pattern.compile("[0-9]{1,2}\\.[0-9]{1}\\.[0-9]{1,2}-MariaDB");
        Pattern phpPattern = Pattern.compile("PHP [0-9]{1,2}\\.[0-9]{1}\\.[0-9]{1,2}");

        String dateTime = null;
        String endDateTime = null;
        String apache;
        String database;
        String php;
        String web_manager;
        String line;

        try {
            DateTimeInfo dateTimeInfo = new DateTimeInfo();


            File folder = new File("/Users/kristina/Desktop/students/");
            if (folder.isDirectory()) {
                for (File file : folder.listFiles()) {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String firstLine = br.readLine(); //Get first line of the text file
                    int firstSpacePosition = firstLine.indexOf(' '); //Get the position of first space
                    String username = firstLine.substring(0, firstSpacePosition); //Username ends at the first space position

                    Matcher dateTimeMatcher = dateTimePattern.matcher(firstLine); //Get the starting date and time
                    if (dateTimeMatcher.find()) {
                        dateTime = (dateTimeMatcher.group(0));
                    }

                    String startTime = dateTime.substring(7, 12); //Cut out the exact starting time
                    String startDate = dateTime.substring(0, dateTime.length() - 6); //Cut out the exact starting date


                    StringBuilder stringBuilder = new StringBuilder();
                    String ls = System.getProperty("line.separator");

                    while ((line = br.readLine()) != null) {
                        stringBuilder.append(line);
                        stringBuilder.append(ls);
                    }

                    String text = stringBuilder.toString(); //Convert the text content to string

                    String formattedDate = dateTimeInfo.formatStartDate(dateTime); //Execute command to convert starting time to number format

                    Matcher endDateTimeMatcher = endTimePattern.matcher(text); //Get the ending date and time
                    if (endDateTimeMatcher.find()) {
                        endDateTime = (endDateTimeMatcher.group(0));
                    }
                    String endTime = endDateTime.substring(6, 11) ; //Cut out the exact ending time
                    Long dateDiffer = dateTimeInfo.calculateTimeDiff(startTime, endTime); //Get time difference from days in minutes

                    String endDate = endDateTime.substring(0, 5); //Cut out the exact ending date
                    Long timeDiffer = dateTimeInfo.CalculateDateDiff(formattedDate, endDate); //Get time difference from hours in minutes


                    String fullTime = Long.toString((dateDiffer + timeDiffer)/60) +
                            "h" + Long.toString((dateDiffer + timeDiffer)%60) + "min";

                    Matcher apacheMatcher = apachePattern.matcher(text);
                    Matcher databaseMatcher = databasePattern.matcher(text);
                    Matcher phpMatcher = phpPattern.matcher(text);

                    if (apacheMatcher.find()) {
                        apache = (apacheMatcher.group(0));
                    } else {
                        apache = "error";
                    }

                    if (databaseMatcher.find()) {
                        database = (databaseMatcher.group(0));
                    } else {
                        database = "error";
                    }

                    if (phpMatcher.find()) {
                        php = (phpMatcher.group(0));
                    } else {
                        php = "error";
                    }

                    if (text.contains("wordpress")) {
                        web_manager = "wordpress";

                    } else if (text.contains("joomla")) {
                        web_manager = "joomla";
                    } else if (text.contains("drupal")) {
                        web_manager = "drupal";

                    } else {
                        web_manager = "error";
                    }

                    student = new Student(username, fullTime, startDate, apache, database, php, web_manager);
                    studentsList.add(student);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return studentsList;
    }


}
