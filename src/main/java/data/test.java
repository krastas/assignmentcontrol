package data;

import config.DbConfig;
import dao.StudentDao;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class test {
    public static void main(String[] args) {

       ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DbConfig.class);

        StudentDao dao = ctx.getBean(StudentDao.class);
            dao.insertStudent();
        System.out.println(dao.getStudentList());
        ctx.close();
    }
}