package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "students")
@NoArgsConstructor
@AllArgsConstructor
public class Student {


    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "student_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Long id;

    @NotNull
    @Size(min = 2)
    @Column(name = "student_name")
    private String studentName;

    @Column(name = "student_time")
    private String studentTime;

    @Size(min = 6)
    @Column(name = "test_date")
    private String testDate;

    @Column(name = "apache")
    private String apache;

    @Column(name = "mariaDB")
    private String mariaDB;

    @Column(name = "php")
    private String php;

    @Column(name = "web_manager")
    private String web_manager;

    public Student(String username, String studentTime, String testDate, String apache, String mariaDB, String php, String web_manager) {
        this.studentName = username;
        this.studentTime = studentTime;
        this.testDate = testDate;
        this.apache = apache;
        this.mariaDB = mariaDB;
        this.php = php;
        this.web_manager = web_manager;
    }

    public Student(String username, String studentTime, String testDate) {
        this.studentName = username;
        this.studentTime = studentTime;
        this.testDate = testDate;
    }
}
