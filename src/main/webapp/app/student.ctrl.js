(function () {
    'use strict';

    angular.module('app').controller('StudentCtrl', Ctrl);

    function Ctrl(http) {

        var vm = this;
        vm.insertStudent = insertStudent;

        vm.newStudent = {};
        vm.students = [];

        init();

       function init() {
            http.get('api/students').then(function (data) {
                vm.students = data;
                vm.errors = [];
            });
        }

        function insertStudent() {
           http.post('api/students', vm.newStudent).then(function () {
               vm.newStudent = {};
               init();
           })

        }
    }
})();